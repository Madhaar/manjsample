//
//  ManjSampleProjectUITests.swift
//  ManjSampleProjectUITests
//
//  Created by Manjinder Singh on 23/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//
  
import XCTest

class ManjSampleProjectUITests: XCTestCase {

    let app = XCUIApplication()
    var appDelegate: AppDelegate?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.

        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        app.launch()
        
        
        
        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testTableExists() {
        let table =  app.tables.firstMatch
        XCTAssertNotNil(table)
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func textTableHasData(){
        let table =  app.tables.firstMatch
        let cells = table.cells
        XCTAssertTrue(cells.count > 0)
        
    }
    
    func testTableInteraction(){
        let articleTableView = app.tables["PostTable"]
        
        XCTAssertTrue(articleTableView.exists, "The Post tableview exists")
        
        // Get an array of cells
        let tableCells = articleTableView.cells
        if tableCells.count > 0 {
            let count: Int = (tableCells.count - 1)
            
            let promise = expectation(description: "Wait for table cells")
            
            for i in stride(from: 0, to: count , by: 1) {
                // Grab the first cell and verify that it exists and tap it
                let tableCell = tableCells.element(boundBy: i)
                XCTAssertTrue(tableCell.exists, "The \(i) cell is in place on the table")
                // Does this actually take us to the next screen
                tableCell.tap()
                
                if i == (count - 1) {
                    promise.fulfill()
                }
                // Back
                app.navigationBars.buttons.element(boundBy: 0).tap()
            }
            waitForExpectations(timeout: 20, handler: nil)
            XCTAssertTrue(true, "Finished validating the table cells")
            
        } else {
            XCTAssert(false, "Was not able to find any table cells")
        }
    }

    func testPostDetailsView() {
        
        // Assert that we are displaying the tableview
        let articleTableView = app.tables["PostTable"]
        
        XCTAssertTrue(articleTableView.exists, "The Post tableview exists")
        
        // Get an array of cells
        let tableCells = articleTableView.cells
        print("Count of table cells \(tableCells.count)")
        
        if tableCells.count > 0 {
            // Grab the first cell and verify that it exists and tap it
            let cellZero = tableCells.element(boundBy: 0)
            XCTAssertTrue(cellZero.exists, "The first cell is in place on the table")
            // Does this actually take us to the next screen
            
            let cellText = cellZero.title
            cellZero.tap()
            // This verifies that our article title label exists
            
            let titleLabel = app.staticTexts["TitleLabel"]
            XCTAssertTrue(titleLabel.exists, "Validating Title Label")
            XCTAssertTrue(titleLabel.title == cellText, "title label is good")
            
            let bodyLabel = app.staticTexts["BodyLabel"]
            XCTAssertTrue(bodyLabel.exists, "Validating Body Label")
            XCTAssertTrue(bodyLabel.title != "Loading", "Body label is good")
            
            let authorLabel = app.staticTexts["AuthorLabel"]
            XCTAssertTrue(authorLabel.exists, "Validating author Label")
            XCTAssertTrue(authorLabel.title != "Loading", "author label is good")
            
            let commentTableView = app.tables["CommentTable"]
            XCTAssertTrue(commentTableView.exists, "The Comment tableview exists")
            
        } else {
            XCTAssert(false, "Was not able to find any table cells")
        }
    }
}
