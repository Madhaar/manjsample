//
//  MockNavController.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 03/03/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

class MockNavigationController: UINavigationController {
    
    var pushedViewController: UIViewController?
    
    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        pushedViewController = viewController
        super.pushViewController(viewController, animated: true)
    }
}
