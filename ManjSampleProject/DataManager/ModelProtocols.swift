//
//  ModelManager.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 24/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

//****Reason of declaring these protocols is:- Tomorrow if underline database changes from CoreData to something else, then that database has to implement these methods

typealias CompletionHandler = () -> Void

protocol PostProtocol {
    func addPosts(_ posts: [PostCodable], completionHandler:@escaping CompletionHandler)
    func getAllPosts() -> Array<PostCodable>
}

protocol CommentProtocol {
    func addComments(_ comments: [CommentCodable],  forPostId: Int, completionHandler:@escaping CompletionHandler)
    func getComments(with postId: Int) -> Array<CommentCodable>
}

protocol UserProtocol {
    func addUsers(_ users: [UserCodable], completionHandler:@escaping CompletionHandler)
    func getUser(with userId: Int) -> UserCodable?
}

protocol ModelProtocol: PostProtocol, CommentProtocol, UserProtocol {
    
}
