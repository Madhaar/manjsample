//
//  DataManager.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 24/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

// This class is managing Data, it serves as a medium between app-Networking and app-model.
/*
 In future if model needs to be replaced from coreData to something else, just changed sharedCoreDataManager.
 */

//let sharedDataManager = DataManager(sharedCoreDataManager)
protocol DataManagerProtocol{
    func getPosts(offlineCompletionHandler:@escaping ([PostCodable]?) -> Void, onlineCompletionHanlder:@escaping ([PostCodable]?) -> Void)
    func getComments(with postId: Int) -> [CommentCodable]?
    func getUser(with userId: Int) -> UserCodable?
    func saveComments(with postId: Int, onlineCompletionHandler: @escaping () -> Void)
    func saveUsers(onlineCompletionHandler: @escaping () -> Void)
}

class DataManager<NP: NetworkProtocol>: DataManagerProtocol {
    
    let networkManager  : NP
    let modelManager    : ModelProtocol
    
    init(_ modelManager: ModelProtocol, networkManager: NP) {
        self.modelManager = modelManager
        self.networkManager = networkManager
    }
}

//Post
extension DataManager{

    func getPosts(offlineCompletionHandler:@escaping ([PostCodable]?) -> Void, onlineCompletionHanlder:@escaping ([PostCodable]?) -> Void){
        weak var weakSelf = self
        offlineCompletionHandler(self.modelManager.getAllPosts())
        self.networkManager.getData(.posts()) {(posts: [PostCodable]?, error) in
            if let postData = posts {
                    weakSelf?.modelManager.addPosts(postData, completionHandler: {
                        onlineCompletionHanlder(weakSelf?.modelManager.getAllPosts())
                    })
            }else{
                 onlineCompletionHanlder(nil)
            }
        }
    }
}

//Comment
extension DataManager{
    
    func saveComments(with postId: Int, onlineCompletionHandler: @escaping () -> Void){
        weak var weakSelf = self
        self.networkManager.getData(.comments(postId: postId)) { (comments: [CommentCodable]?, error) in
            if comments != nil{
                weakSelf?.modelManager.addComments(comments!, forPostId: postId, completionHandler: {
                    onlineCompletionHandler()
                })
            }else{
                onlineCompletionHandler()
            }
        }
    }
    
    func getComments(with postId: Int) -> [CommentCodable]?{
       return self.modelManager.getComments(with:postId)
    }
}

//User
extension DataManager{
    
    func saveUsers(onlineCompletionHandler: @escaping () -> Void){
        weak var weakSelf = self
        self.networkManager.getData(.users()) {(users: [UserCodable]?, error) in
            
            if  users != nil{
                weakSelf?.modelManager.addUsers(users!, completionHandler: {
                    onlineCompletionHandler()
                })
            }
        }
    }
    
    func getUser(with userId: Int) -> UserCodable?{
      return  self.modelManager.getUser(with: userId)
    }
}
