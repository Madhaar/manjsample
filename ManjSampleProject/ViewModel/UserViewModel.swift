//
//  UserViewModel.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 30/03/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

struct UserViewModel {
    
    private let user: UserCodable
    
    init(user: UserCodable) {
        self.user = user
    }
    
    var name: String{
        return self.user.name
    }
}
