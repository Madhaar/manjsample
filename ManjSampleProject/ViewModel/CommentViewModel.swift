//
//  CommentViewModel.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 30/03/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

struct CommentViewModel {
    
    private let comment: CommentCodable
    
    init(comment: CommentCodable) {
        self.comment = comment
    }
    
    var name: String{
        if comment.name != nil {
            return comment.name!.capitalizingFirstLetter()
        }
        return ""
    }
    
    var id: Int{
        return comment.id
    }
    
    var postId: Int{
        return comment.postId
    }
    
    var body: String{
        if comment.body != nil {
            return comment.body!.capitalizingFirstLetter()
        }
        return ""
    }
    
    var email: String{
        if comment.email != nil {
            return comment.email!
        }
        return ""
    }
}
