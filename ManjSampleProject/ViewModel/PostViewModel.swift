//
//  PostViewModel.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 30/03/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

struct PostViewModel{
    
    private let post: PostCodable
    
    init(post: PostCodable) {
        self.post = post
    }
    
    var title: String{
        if post.title != nil {
           return post.title!.capitalizingFirstLetter()
        }
        return ""
    }
   
    var id: Int{
        return post.id
    }
    
    var userId: Int{
        return post.userId
    }
    
    var body: String{
        
        if post.body != nil {
            return post.body!.capitalizingFirstLetter()
        }
        return ""
    }
    
}
