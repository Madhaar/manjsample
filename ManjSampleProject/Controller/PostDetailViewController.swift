//
//  PostDetailsViewController.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 23/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

class PostDetailViewController: UIViewController {
    
    // MARK: - IBOutlets
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var bodyLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var commentTableView: TWTableView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
    @IBOutlet weak var myScroller: UIScrollView!
    
    // MARK: - iVars
    let post: PostViewModel
    let dataManager: DataManagerProtocol
    
    var commentsArray = [CommentCodable]()
    let cellIdentifier = "commentsCellIdentifier"
    
    // MARK: - Load View
    init(post: PostViewModel, dataManager: DataManagerProtocol) {
        self.post = post
        self.dataManager = dataManager
        super.init(nibName: "PostDetailViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        commentTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        commentTableView.delegate = self
        commentTableView.dataSource = self
        
        loadData()
        settingAccessibility()
    }
    
    func loadData(){
        titleLabel.text = post.title
        bodyLabel.text = post.body
        let commentBlock: (_ comments: [CommentCodable]) -> Void =  {[weak self] comments in
            guard comments.count > 0 else{
                return
            }
            self?.commentsArray = comments.sorted{$0.id < $1.id}
            DispatchQueue.main.async {
                self?.commentTableView.reloadData()
            }
        }
        
        if let comments =   self.dataManager.getComments(with: post.id){
            commentBlock(comments)
        }
        
        if let user =  self.dataManager.getUser(with: Int(post.userId)){
            authorLabel.text =  UserViewModel(user: user).name
        }
        
        weak var weakSelf = self
        self.dataManager.saveComments(with: post.id) {
            if let comments =   weakSelf?.dataManager.getComments(with: (weakSelf?.post.id)!){
                commentBlock(comments)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if commentTableView.frame.origin.y + 300 < UIScreen.main.bounds.size.height {
            let diff = UIScreen.main.bounds.size.height - (commentTableView.frame.origin.y + 300)
            heightConstraint.constant = 300 + diff
        }
    }
}

extension PostDetailViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - TableView Data Source and Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.selectionStyle = .none
        let comment = CommentViewModel(comment: commentsArray[indexPath.row])
        cell.textLabel?.text = comment.body
        cell.textLabel?.font = UIFont(name: "Helvetica", size: 14)
        cell.textLabel?.numberOfLines = 0
        return cell
    }
}

extension PostDetailViewController{
    func settingAccessibility(){
        titleLabel.accessibilityIdentifier          = "TitleLabel"
        bodyLabel.accessibilityIdentifier           = "BodyLabel"
        authorLabel.accessibilityIdentifier         = "AuthorLabel"
        commentTableView.accessibilityIdentifier    = "CommentTable"
    }
}

