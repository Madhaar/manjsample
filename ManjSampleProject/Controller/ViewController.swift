//
//  ViewController.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 23/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var postsTableView: UITableView!
    let cellIdentifier = "postCellIdentifier"

    var dataManager: DataManagerProtocol
    var postsArray = [Any]()
    
    init(dataManager: DataManagerProtocol) {
        self.dataManager = dataManager
        super.init(nibName: "ViewController", bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        postsTableView.register(UITableViewCell.self, forCellReuseIdentifier: cellIdentifier)
        postsTableView.delegate = self
        postsTableView.dataSource = self
        postsTableView.accessibilityIdentifier = "PostTable"
        loadData()
    }
    
    func loadData(){
    
        let block: (_ posts: [PostCodable]) -> Void = {[weak self] posts in
            self?.postsArray = posts
            DispatchQueue.main.async {
                self?.postsTableView.reloadData()
            }
        }
        
        dataManager.getPosts(offlineCompletionHandler: {[weak self] (posts) in
            if posts != nil{
                block(posts!)
            }else{
                if self != nil{
                    self!.showSpinner(onView: self!.view)
                }
            }
        }) {[weak self] (posts) in
            self?.removeSpinner()
            if posts != nil{
                
                block(posts!)
            }
        }
        
        dataManager.saveUsers {}
        
    }
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - TableView Data Source and Delegate
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.postsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier, for: indexPath)
        cell.selectionStyle = .none
        cell.textLabel?.font = UIFont(name: "Helvetica", size: 14)
        cell.textLabel?.numberOfLines = 0
        if let post = postsArray[indexPath.row]  as? PostCodable{
            let postViewModel = PostViewModel(post: post)
            cell.textLabel?.text = postViewModel.title
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let post = postsArray[indexPath.row]  as? PostCodable{
            self.cellSelectedWithPost(PostViewModel(post: post))
        }
    }
    
    func cellSelectedWithPost(_ post: PostViewModel){
        
        self.navigationController?.pushViewController(PostDetailViewController(post: post, dataManager: dataManager), animated: true)
    }
}

var mySpinner : UIView?
extension UIViewController {
    func showSpinner(onView : UIView) {
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(style: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        
        DispatchQueue.main.async {
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        mySpinner = spinnerView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            mySpinner?.removeFromSuperview()
            mySpinner = nil
        }
    }
}

extension String {
    func capitalizingFirstLetter() -> String {
        return prefix(1).capitalized + dropFirst()
    }
    
    mutating func capitalizeFirstLetter() {
        self = self.capitalizingFirstLetter()
    }
}
