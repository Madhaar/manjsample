//
//  TWTableView.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 03/03/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import UIKit

class TWTableView: UITableView {
    var maxHeight: CGFloat = UIScreen.main.bounds.size.height
    
    override func reloadData() {
        super.reloadData()
        self.invalidateIntrinsicContentSize()
        self.layoutIfNeeded()
    }
    
    override var intrinsicContentSize: CGSize {
        setNeedsLayout()
        layoutIfNeeded()
        let height = min(contentSize.height, maxHeight)
        return CGSize(width: contentSize.width, height: height)
    }
}
