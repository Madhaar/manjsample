//
//  PostEndPoint.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 23/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

enum NetworkEnvironment {
    case qa
    case production
    case staging
}

public enum PostAPI{
    case posts()
    case comments(postId: Int)
    case users()
}

extension PostAPI: EndPointType{
    // Just a way to show you can have different enviorment set here. 
    var environmentBaseURL : String {
        switch NetworkManager.environment {
        case .production: return "http://jsonplaceholder.typicode.com/"
        case .qa: return "http://jsonplaceholder.typicode.com/"
        case .staging: return "http://jsonplaceholder.typicode.com/"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        switch self {
        case .posts():
            return "posts"
        case .comments(let postId):
            let str = "comments?postId=\(postId)"

            return str
        case .users():
            return "users"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        
        case .posts():
            return .request
        case .comments(let postId):
            return .request
        case .users():
            return .request
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
    
    
}
