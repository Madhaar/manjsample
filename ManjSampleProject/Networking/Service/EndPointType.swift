//
//  EndPointType.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 23/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

protocol EndPointType {
    var baseURL: URL { get }
    var path: String { get }
    var httpMethod: HTTPMethod { get }
    var task: HTTPTask { get }
    var headers: HTTPHeaders? { get }
}
