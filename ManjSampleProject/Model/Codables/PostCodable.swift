//
//  PostCodable.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 24/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

public struct PostCodable: Codable {
    var userId: Int
    var id: Int
    var title: String?
    var body: String?
    
   public init(from decoder: Decoder) throws{

        let container = try decoder.container(keyedBy: CodingKeys.self)
        userId = try container.decode(Int.self, forKey: .userId)
        id = try container.decode(Int.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
        body = try container.decode(String.self, forKey: .body)
    }
}
