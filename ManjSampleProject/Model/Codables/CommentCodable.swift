//
//  CommentCodable.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 02/03/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

public struct CommentCodable: Codable{
    
    let id: Int
    let postId: Int
    let body: String?
    let name: String?
    let email: String?
    
  public init(from decoder: Decoder) throws{
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(Int.self, forKey: .id)
        postId = try container.decode(Int.self, forKey: .postId)
        body = try container.decode(String.self, forKey: .body)
        name = try container.decode(String.self, forKey: .name)
        email = try container.decode(String.self, forKey: .email)
    }
}
