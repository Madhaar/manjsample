//
//  UserCodable.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 03/03/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

public struct UserCodable: Codable {
    
    var name: String
    var id: Int
    var email: String?
    var username: String?
    
    public init(from decoder: Decoder) throws{
        
        let container = try decoder.container(keyedBy: CodingKeys.self)
        username = try container.decode(String.self, forKey: .username)
        id = try container.decode(Int.self, forKey: .id)
        email = try container.decode(String.self, forKey: .email)
        name = try container.decode(String.self, forKey: .name)
    }
}
