//
//  CoreDataHandler.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 24/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import CoreData
import UIKit

let sharedCoreDataManager = CoreDataManager()

class CoreDataManager: ModelProtocol {
    
    let context: NSManagedObjectContext
    let persistentContainer: NSPersistentContainer!
    let persistentContainerQueue = OperationQueue();
    
    //MARK: Init with dependency
    init(container: NSPersistentContainer) {
        self.persistentContainer = container
        self.persistentContainer.viewContext.automaticallyMergesChangesFromParent = true
         persistentContainerQueue.maxConcurrentOperationCount = 1
        context  =  self.persistentContainer.viewContext
    }
    
    convenience init() {
        //Use the default container for production environment

        self.init(container: sharedCoreDataBase.persistentContainer)
    }
    
    func saveContext(context: NSManagedObjectContext, completion: @escaping () -> Void) {
        do {
            try context.save()
            completion()
        } catch {
            fatalError("Failure to save context: \(error)")
           // completion()
        }
    }
}

extension CoreDataManager{
    
    func addPosts(_ posts: [PostCodable], completionHandler: @escaping () -> Void) {
        self.persistentContainer.performBackgroundTask({ (backgroundContext) in
            backgroundContext.mergePolicy = NSMergePolicy(merge: NSMergePolicyType.overwriteMergePolicyType)
            
            for post in posts {
                if let entity = NSEntityDescription.entity(forEntityName: "Post", in: backgroundContext){
                    let newPost = NSManagedObject(entity: entity, insertInto: backgroundContext)
                    
                    // Set the data to the entity
                    newPost.setValue(post.id, forKey: "id")
                    newPost.setValue(post.userId, forKey: "userId")
                    newPost.setValue(post.title, forKey: "title")
                    newPost.setValue(post.body, forKey: "body")
                }
            }
            self.saveContext(context: backgroundContext, completion: {
                completionHandler()
            })
        })
        
    }
    
    func getAllPosts() -> Array<PostCodable> {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Post")
        request.resultType = .dictionaryResultType
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        
        var allPosts = [PostCodable]()
        
        do {
            let fetched = try context.fetch(request)
            let jsonData = try JSONSerialization.data(withJSONObject: fetched, options: .prettyPrinted)
           allPosts =  try JSONDecoder().decode(Array<PostCodable>.self, from: jsonData)
        } catch {
            let nserror = error as NSError
            //TODO: Handle Error
            print(nserror.description)
        }
        return allPosts
    }

    func getPostModel(predicate: NSPredicate, withConext: NSManagedObjectContext) -> Post?{
        let request = NSFetchRequest<Post>(entityName: "Post")
        request.predicate = predicate
        var post: Post?
        
        do {
            let fetched = try withConext.fetch(request)
            if fetched.count > 0 {
                 post = fetched.first
            }
           
        } catch {
            let nserror = error as NSError
            //TODO: Handle Error
            print(nserror.description)
        }
        return post
    }
    
    //Unused method
    func getPost(predicate: NSPredicate, withConext: NSManagedObjectContext) -> PostCodable?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Post")
        request.predicate = predicate
        var post: PostCodable?
        
        do {
            let fetched = try withConext.fetch(request)
            if fetched.count > 0{
                let jsonData = try JSONSerialization.data(withJSONObject: fetched.first!, options: .prettyPrinted)
                post =  try JSONDecoder().decode(PostCodable.self, from: jsonData)
            }
        } catch {
            let nserror = error as NSError
            //TODO: Handle Error
            print(nserror.description)
        }
        return post
    }
}

extension CoreDataManager {
    func addComments(_ comments: [CommentCodable], forPostId: Int,  completionHandler: @escaping CompletionHandler) {
        
        self.persistentContainer.performBackgroundTask({ (backgroundContext) in
            backgroundContext.mergePolicy = NSMergePolicy(merge: NSMergePolicyType.mergeByPropertyObjectTrumpMergePolicyType)
            if  let post = self.getPostModel(predicate: NSPredicate(format: "id == \(NSNumber(integerLiteral: forPostId))"), withConext: backgroundContext){
                
                for comment in comments {
                    if let entity = NSEntityDescription.entity(forEntityName: "Comment", in: backgroundContext){
                        let newComment = NSManagedObject(entity: entity, insertInto: backgroundContext)
                        
                        newComment.setValue(comment.id, forKey: "id")
                        newComment.setValue(comment.postId, forKey: "postId")
                        newComment.setValue(comment.email, forKey: "email")
                        newComment.setValue(comment.body, forKey: "body")
                        newComment.setValue(comment.name, forKey: "name")
                        post.addToComments(newComment as! Comment)
                    }
                }
            }
            self.saveContext(context: backgroundContext, completion: {
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) { // Change `2.0` to the desired number of
                    completionHandler()
                }
               
            })
        })
    }
    
    func getComments(with postId: Int) -> Array<CommentCodable> {
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Comment")
        request.resultType = .dictionaryResultType
        request.predicate = NSPredicate( format: "postId == %@", NSNumber(integerLiteral: postId))
        request.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        var allComments = [CommentCodable]()
        
        do {
            let fetched = try context.fetch(request)
            if fetched.count > 0{
                let jsonData = try JSONSerialization.data(withJSONObject: fetched, options: .prettyPrinted)
                allComments =  try JSONDecoder().decode(Array<CommentCodable>.self, from: jsonData)
            }
        } catch {
            let nserror = error as NSError
            //TODO: Handle Error
            print(nserror.description)
        }
        
        return allComments
    }
}

extension CoreDataManager {
    
    func addUsers(_ users: [UserCodable], completionHandler: @escaping CompletionHandler) {
        self.persistentContainer.performBackgroundTask({ (backgroundContext) in
            backgroundContext.mergePolicy = NSMergePolicy(merge: NSMergePolicyType.overwriteMergePolicyType)
            for user in users {
                if let entity = NSEntityDescription.entity(forEntityName: "User", in: backgroundContext){
                    let newUser = NSManagedObject(entity: entity, insertInto: backgroundContext)
                    
                    newUser.setValue(user.id, forKey: "id")
                    newUser.setValue(user.email, forKey: "email")
                    newUser.setValue(user.username, forKey: "username")
                    newUser.setValue(user.name, forKey: "name")
                }
            }
            self.saveContext(context: backgroundContext, completion: {
                completionHandler()
            })
            
        })
    }
    
    func getUser(with userId: Int) -> UserCodable?{
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "User")
        request.resultType = .dictionaryResultType
        request.predicate =  NSPredicate( format: "id == \(NSNumber(integerLiteral: userId))")
        var user: UserCodable?
        
        do {
            let fetched = try context.fetch(request)
            if fetched.count > 0{
                let jsonData = try JSONSerialization.data(withJSONObject: fetched, options: .prettyPrinted)
                let users =  try JSONDecoder().decode(Array<UserCodable>.self, from: jsonData)
                user = users.first!
            }
        } catch {
            let nserror = error as NSError
            //TODO: Handle Error
            print(nserror.description)
        }
        
        return user
    }
}
