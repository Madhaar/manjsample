//
//  CodableTests.swift
//  ManjSampleProjectTests
//
//  Created by Manjinder Singh on 30/03/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import XCTest

class CodableTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testPostCodableGoodData(){
        XCTAssertNoThrow(try JSONDecoder().decode([PostCodable].self, from: expectedPostData!))
    }
    
    func testPostCodableBadData(){
        
        AssertThrowsKeyNotFound("userId", decoding: Array<PostCodable>.self, from: unExpectedPostData)
    }
    
    func testCommentCodableGoodData(){
        XCTAssertNoThrow(try JSONDecoder().decode([CommentCodable].self, from: expectedCommentData!))
    }
    
    func testCommentCodableBadData(){
        
        AssertThrowsKeyNotFound("email", decoding: Array<CommentCodable>.self, from: unExpectedCommentData!)
    }
  
    func testUserCodableGoodData(){
        XCTAssertNoThrow(try JSONDecoder().decode([UserCodable].self, from: expectedUserData!))
    }
    
    func testUserCodableBadData(){
        
        AssertThrowsKeyNotFound("username", decoding: Array<UserCodable>.self, from: unExpectedUserData!)
    }
    
    func AssertThrowsKeyNotFound<T: Decodable>(_ expectedKey: String, decoding: T.Type, from data: Data, file: StaticString = #file, line: UInt = #line) {
        XCTAssertThrowsError(try JSONDecoder().decode(decoding, from: data), file: file, line: line) { error in
            if case .keyNotFound(let key, _)? = error as? DecodingError {
                XCTAssertEqual(expectedKey, key.stringValue, "Expected missing key '\(key.stringValue)' to equal '\(expectedKey)'.", file: file, line: line)
            } else {
                XCTFail("Expected '.keyNotFound(\(expectedKey))' but got \(error)", file: file, line: line)
            }
        }
    }
    
}
