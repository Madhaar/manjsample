//
//  ManjSampleProjectTests.swift
//  ManjSampleProjectTests
//
//  Created by Manjinder Singh on 23/02/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import XCTest
@testable import ManjSampleProject

class ManjSampleProjectTests: XCTestCase {
    
    let session = MockURLSession()
    var networkManager: NetworkManager?
    var coreDataManager: CoreDataManager?
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        continueAfterFailure = false
        networkManager = NetworkManager(session)
        coreDataManager = CoreDataManager(container: MockModelManager().mockPersistantContainer)
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testDataIsReturned(){
        
        session.nextData = expectedPostData
        var actualData: Any?
        networkManager?.getData(.posts(), callback: { (posts: [PostCodable]?, error) in
            actualData = posts
            XCTAssertNotNil(actualData)
        })
    }
    
    func testCommentData(){
        
        session.nextData = expectedCommentData
        var actualData: Any?
        networkManager?.getData(PostAPI.comments(postId: 1), callback: { (comments: [CommentCodable]?, error) in
            actualData = comments
            XCTAssertNotNil(actualData)
        })
    }
    
    func testResumeCalled(){
        let dataTask  = MockURLSessionDataTask()
        session.nextDataTask = dataTask
        networkManager?.getData(.posts(), callback: { (posts: [PostCodable]?, error) in
        })
        
        XCTAssert(dataTask.resumeWasCalled)
    }
    
    func testPostViewModel(){
        do{
            let data = try JSONDecoder().decode([PostCodable].self, from: expectedPostData!)
            let postVM = PostViewModel(post: data.first!)
            XCTAssertNotNil(postVM)
            XCTAssertTrue(postVM.title == "Hello")
            XCTAssertTrue(postVM.id == 1)
            XCTAssertTrue(postVM.userId == 1)
            XCTAssertTrue(postVM.body == "I am a body")
        }catch{
            XCTFail("Post Json is not parsed")
        }
    }
    
    func testCommentViewModel(){
        do{
            let data = try JSONDecoder().decode([CommentCodable].self, from: expectedCommentData!)
            let commentVM = CommentViewModel(comment: data.first!)
            XCTAssertNotNil(commentVM)
            XCTAssertTrue(commentVM.postId == 1)
            XCTAssertTrue(commentVM.id == 1)
            XCTAssertTrue(commentVM.email == "m@y.com" )
            XCTAssertTrue(commentVM.body == "I am a body")
            XCTAssertTrue(commentVM.name == "Manj")
            
        }catch{
            XCTFail("Comment Json is not parsed")
        }
    }
    
    func testUserViewModel(){
        do{
            let data = try JSONDecoder().decode([UserCodable].self, from: expectedUserData!)
            let userVM = UserViewModel(user: data.first!)
            XCTAssertNotNil(userVM)
            XCTAssertTrue(userVM.name == "Mack")
        }catch{
            XCTFail("User Json is not parsed")
        }
    }
    
    func testViewController(){
        let vc =  ViewController(dataManager: DataManager(coreDataManager!, networkManager: networkManager!))
        vc.loadView()
        vc.viewDidLoad()
        XCTAssertNotNil(vc.postsTableView)
    }
    
    func testPostDetailsVC(){
        do{
            
            let data = try JSONDecoder().decode([PostCodable].self, from: expectedPostData!)
            let postCod = data.first!
            let postVC = PostDetailViewController(post: PostViewModel(post: postCod), dataManager: DataManager(coreDataManager!, networkManager: networkManager!))
            
            let users = try JSONDecoder().decode([UserCodable].self, from: expectedUserData!)
            let comments = try JSONDecoder().decode([CommentCodable].self, from: expectedCommentData!)
            
            let blockOperation = BlockOperation()
           
            blockOperation.addExecutionBlock {
                self.coreDataManager?.addComments(comments, forPostId: postVC.post.id, completionHandler: {
                    
                })
            }
            blockOperation.addExecutionBlock {
                self.coreDataManager?.addUsers(users, completionHandler: {
    
                })
            }
            
            blockOperation.completionBlock = {
                DispatchQueue.main.async(execute: {
                    postVC.loadView()
                    postVC.viewDidLoad()
                    XCTAssert( postVC.authorLabel.text != "Loading")
                    XCTAssertNotNil( postVC.commentTableView)
                    
                })
            
            }
            
            let queue = OperationQueue()
            queue.addOperation(blockOperation)
            queue.waitUntilAllOperationsAreFinished()
            
        }catch{
            XCTFail("Post Json is not parsed")
        }
    }
    
}
