//
//  CoreDataTests.swift
//  ManjSampleProjectTests
//
//  Created by Manjinder Singh on 31/03/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import XCTest
import CoreData

class CoreDataTests: XCTestCase {

    var sut: CoreDataManager!
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        sut = CoreDataManager(container: MockModelManager().mockPersistantContainer)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testPosts() {
        do{
            let data: [PostCodable] = try JSONDecoder().decode([PostCodable].self, from: expectedPostData!)
            sut.addPosts(data) {
                guard data.count > 0 else{
                    XCTFail("Posts are not saved")
                    return
                }
                let post =   self.sut.getPost(predicate: NSPredicate(format: "id BEGINSWITH %@",NSNumber(integerLiteral: data.first!.id)), withConext: self.sut.context)
                XCTAssertNotNil(post)
            }
        }catch{
            XCTFail("Post Json is not parsed")
        }
    }
    
}
