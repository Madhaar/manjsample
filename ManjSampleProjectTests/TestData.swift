//
//  TestData.swift
//  ManjSampleProject
//
//  Created by Manjinder Singh on 30/03/2019.
//  Copyright © 2019 Manjinder Singh. All rights reserved.
//

import Foundation

let expectedPostData = "[{\"userId\": 1,\"id\": 1,\"title\": \"hello\",\"body\": \"I am a body\"}]".data(using: .utf8)

 let unExpectedPostData = Data("""
[
    {
    "userIdf": 1,
    "id": 1,
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "hello there"
  }
]
""".utf8)

let expectedCommentData = "[{\"postId\": 1,\"id\": 1,\"name\": \"Manj\",\"body\": \"I am a body\",\"email\": \"m@y.com\"}]".data(using: .utf8)

let unExpectedCommentData = "[{\"postId\": 1,\"id\": 1,\"name\": \"postName\",\"body\": \"I am a body\",\"emailID\": \"m@y.com\"}]".data(using: .utf8)

let expectedUserData = "[{\"name\": \"Mack\",\"id\": 1,\"email\": \"post@y.com\",\"username\": \"Heri\"}]".data(using: .utf8)

let unExpectedUserData = "[{\"name\": \"Mack\",\"id\": 1,\"email\": \"post@y.com\",\"userName\": \"Heri\"}]".data(using: .utf8)
